# README #

This package is for running the semantic SLAM algorithm (under development).

### How do I get set up? ###

This package depends on particle filter ROS package which can be downloaded from the following link:
https://bitbucket.org/hridaybavle/particle_filter

Make sure you have this package in the same ROS workspace.
