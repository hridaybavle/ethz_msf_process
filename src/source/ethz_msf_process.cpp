#include "ethz_msf_process.h"

ethz_msf_process_ros::ethz_msf_process_ros():
    RobotProcess(), sync (SyncPolicy(10)), sync_imu_vo (SyncPolicyIMUVO(10))
{
    std::cout << "ethz_msf_process_ros constructor " << std::endl;
}

ethz_msf_process_ros::~ethz_msf_process_ros()
{
    std::cout << "ethz_msf_process_ros destructor " << std::endl;
}


void ethz_msf_process_ros::ownSetUp()
{
    //assign all the variables here

    //initializing the filter
    this->init();
}

void ethz_msf_process_ros::ownStop()
{
    //assign all the variables to default here


}

void ethz_msf_process_ros::ownRun()
{
    //run the algorithm over here

}

void ethz_msf_process_ros::init()
{

    dji_first_yaw_measurement_ = false;
    is_first_vo_z_msg_         = false;
    start_calcualating_dt_     = false;
    dji_first_roll_ =0; dji_first_pitch_ =0; dji_first_yaw_ =0;
    prev_time_ = 0; current_time_ = 0;
    vo_position_x_ = 0; vo_position_y_ = 0; vo_position_z_=0.3;
    imu_data_.reset(new sensor_msgs::Imu);
    dji_current_sonar_height_ = 0;

    msf_odom_.pose.pose.position.x = 0; msf_odom_.pose.pose.position.y = 0; msf_odom_.pose.pose.position.z = 0;
    msf_odom_.pose.pose.orientation.x = 0; msf_odom_.pose.pose.orientation.y = 0; msf_odom_.pose.pose.orientation.z = 0; msf_odom_.pose.pose.orientation.w = 1;
    msf_odom_.twist.twist.linear.x = 0; msf_odom_.twist.twist.linear.y = 0; msf_odom_.twist.twist.linear.z = 0;
    msf_odom_.twist.twist.angular.x = 0; msf_odom_.twist.twist.angular.y = 0; msf_odom_.twist.twist.angular.z = 0;

    ros::param::param<bool>("~use_global_positioning_data", use_global_positioning_data_, false);
    ros::param::param<bool>("~use_vo_data", use_vo_data_, false);
    ros::param::param<bool>("~use_rtk_position", use_rtk_position_, false);
    ros::param::param<bool>("~use_rtk_pose", use_rtk_pose_, false);
    ros::param::param<bool>("~use_gps_pose", use_gps_pose_, false);
    ros::param::param<bool>("~use_optitrack_data", use_optitrack_data_, false);
    ros::param::param<bool>("~use_geodesic_yaw", use_geodesic_yaw_, false);


    std::cout << "using global positioning data: " << use_global_positioning_data_ << std::endl;
    std::cout << "using VO data: " << use_vo_data_ << std::endl;
    std::cout << "using rtk position: " << use_rtk_position_ << std::endl;
    std::cout << "using rtk pose: " << use_rtk_pose_ << std::endl;
    std::cout << "using gps pose: " << use_gps_pose_ << std::endl;
    std::cout << "using optitrack data: " << use_optitrack_data_ << std::endl;
    std::cout << "using geodesic yaw: " << use_geodesic_yaw_ << std::endl;


}

void ethz_msf_process_ros::ownStart()
{
    //start all the subscribers and publishers over here
    dji_velocity_sub_         = n.subscribe("dji_sdk/velocity",1, &ethz_msf_process_ros::velocityCallback, this);
    dji_imu_sub_              = n.subscribe("dji_sdk/imu",1,&ethz_msf_process_ros::imuCallback, this);
    dji_height_sub_           = n.subscribe("dji_sdk/height_above_takeoff",1,&ethz_msf_process_ros::DjiHeigtCallback, this);
    msf_odom_sub_             = n.subscribe("/msf_core/odometry", 1, &ethz_msf_process_ros::msfOdomCallback, this);
    if(use_geodesic_yaw_)
        geodesic_first_yaw_sub_   = n.subscribe("geodesic_yaw",1, &ethz_msf_process_ros::geodesicFirstYawCallback, this);


    if(use_global_positioning_data_)
    {
        if(use_vo_data_)
            ROS_WARN("Tried to activate gps pose data and VO pose data which is prohibited, not publishing gps data");
        else
        {
            //time synchronization of GPS and imu
            dji_imu_snyn_sub_.subscribe(n, "dji_sdk/imu",1);
            if(use_rtk_pose_)
            {
                gps_enu_sub_.subscribe(n, "rtk_position_enu",1);
                ROS_INFO("Subscribing to RTK Pose data");
            }
            else if (use_gps_pose_)
            {
                gps_enu_sub_.subscribe(n, "gps_position_enu",1);
                ROS_INFO("Subscribing to GPS Pose data");

            }
            if(use_rtk_pose_ && use_gps_pose_)
            {
                ROS_WARN("Tried to activate both gps pose data and rtk pose data which is prohibited, publishing only gps data");
                gps_enu_sub_.subscribe(n, "gps_position_enu",1);
            }
            sync.connectInput(dji_imu_snyn_sub_,gps_enu_sub_);
            sync.registerCallback(boost::bind(&ethz_msf_process_ros::imuGPSSyncCallback, this, _1, _2));

            if(use_rtk_position_)
            {
                dji_rtk_sub_ = n.subscribe("rtk_position_enu",1, &ethz_msf_process_ros::djiRTKCallback, this);
                dji_rtk_pub_ = n.advertise<geometry_msgs::PointStamped>("dji_rtk",1);
            }
        }
    }
    else if(use_optitrack_data_)
    {
        if(use_vo_data_)
            ROS_WARN("Tried to activate optitrack pose data and VO pose data which is prohibited, not publishing optitrack data");
        else if(use_rtk_pose_ || use_gps_pose_)
            ROS_WARN("Tried to activate optitrack pose data and gps/rtk pose data which is prohibited, not publishing optitrack data");
        else
            optitrack_odom_sub_  = n.subscribe("/vrpn_client_node/m210/pose",1, &ethz_msf_process_ros::optitrackOdomCallback, this);
    }

    dji_pose_pub_                = n.advertise<geometry_msgs::PoseStamped>("dji_pose",1);
    velocity_pub_                = n.advertise<geometry_msgs::PointStamped>("velocity", 1);
    altitude_above_takeoff_pub_  = n.advertise<geometry_msgs::PointStamped>("altitude_above_takeoff",1);

    msf_odom_pub_               = n.advertise<nav_msgs::Odometry>("msf_interface/odometry",1);
    aerostack_pose_pub_         = n.advertise<ethz_msf_process::dronePose>("EstimatedPose_droneGMR_wrt_GFF",1);
    aerostack_speed_pub_        = n.advertise<ethz_msf_process::droneSpeeds>("EstimatedSpeed_droneGMR_wrt_GFF",1);

    //thread for publishing msf at given hz
    msf_odom_pub_thread = new std::thread(&ethz_msf_process_ros::publishMSFOdom, this);

    //waiting for the msf to be launched first
    usleep(5000000);
    //starting the ethz msf
    dynamic_reconfigure::ReconfigureRequest srv_req;
    dynamic_reconfigure::ReconfigureResponse srv_resp;
    dynamic_reconfigure::BoolParameter bool_param;
    dynamic_reconfigure::Config conf;

    bool_param.name = "core_init_filter";
    bool_param.value = true;
    conf.bools.push_back(bool_param);

    srv_req.config = conf;
    ros::service::call("/msf_pose_position_velocity_altitude_sensor/pose_velocity_altitude_sensor/set_parameters", srv_req, srv_resp);

}

void ethz_msf_process_ros::velocityCallback(const geometry_msgs::Vector3Stamped &msg)
{
    if(!dji_first_yaw_measurement_)
        return;

    Eigen::Vector3f local_world_frame = this->ConvertToENU(msg);
    current_time_ = (double) ros::Time::now().sec + ((double) ros::Time::now().nsec / (double) 1E9);

    geometry_msgs::PointStamped velocity_world;
    velocity_world.header    = msg.header;
    velocity_world.point.x   = local_world_frame(0);
    velocity_world.point.y   = local_world_frame(1);
    velocity_world.point.z   = msg.vector.z;
    this->publishVelocity(velocity_world);

    if (use_vo_data_)
    {
        if(!start_calcualating_dt_)
        {
            start_calcualating_dt_ = true;
            prev_time_ = current_time_ ;
            return;
        }

        double time_diff = current_time_ - prev_time_;

        sensor_msgs::ImuConstPtr curr_imu_data;
        this->getIMUdata(curr_imu_data);
        tf::Quaternion quaternion = this->getCorrectYaw(curr_imu_data);

        vo_position_x_ = vo_position_x_ + local_world_frame(0)*time_diff;
        vo_position_y_ = vo_position_y_ + local_world_frame(1)*time_diff;
        vo_position_z_ = vo_position_z_ + msg.vector.z*time_diff;

        geometry_msgs::PoseStamped vo_pose;
        vo_pose.header.stamp    = msg.header.stamp;
        vo_pose.header.frame_id = msg.header.frame_id;
        vo_pose.pose.position.x = vo_position_x_;
        vo_pose.pose.position.y = vo_position_y_;
        vo_pose.pose.position.z = vo_position_z_;
        vo_pose.pose.orientation.x = quaternion.getX();
        vo_pose.pose.orientation.y = quaternion.getY();
        vo_pose.pose.orientation.z = quaternion.getZ();
        vo_pose.pose.orientation.w = quaternion.getW();

        this->publishDJIPose(vo_pose);
        prev_time_ = current_time_;

    }
}

void ethz_msf_process_ros::imuGPSSyncCallback(const sensor_msgs::ImuConstPtr &imu_msg,
                                              const nav_msgs::OdometryConstPtr& gps_msg)
{
    if(!dji_first_yaw_measurement_)
        return;

    geometry_msgs::PoseStamped gps_imu_pose;
    gps_imu_pose.header.stamp = imu_msg->header.stamp;
    gps_imu_pose.header.frame_id = gps_msg->header.frame_id;

    if(use_rtk_pose_)
    {
        //adding position data
        gps_imu_pose.pose.position.x = gps_msg->pose.pose.position.x;
        gps_imu_pose.pose.position.y = gps_msg->pose.pose.position.y;
        gps_imu_pose.pose.position.z = gps_msg->pose.pose.position.z+0.30; //30cm from floor to base link
    }
    else if(use_gps_pose_)
    {
        gps_imu_pose.pose.position.x = gps_msg->pose.pose.position.x;
        gps_imu_pose.pose.position.y = gps_msg->pose.pose.position.y;
        gps_imu_pose.pose.position.z = dji_current_sonar_height_;
    }

    tf::Quaternion quaternion = this->getCorrectYaw(imu_msg);

    //adding orientation data
    gps_imu_pose.pose.orientation.x = quaternion.getX();
    gps_imu_pose.pose.orientation.y = quaternion.getY();
    gps_imu_pose.pose.orientation.z = quaternion.getZ();
    gps_imu_pose.pose.orientation.w = quaternion.getW();

    this->publishDJIPose(gps_imu_pose);
}


void ethz_msf_process_ros::djiRTKCallback(const nav_msgs::OdometryConstPtr &msg)
{

    geometry_msgs::PointStamped rtk_position;
    rtk_position.header.stamp = msg->header.stamp;
    rtk_position.header.frame_id = msg->header.frame_id;
    rtk_position.point.x = msg->pose.pose.position.x;
    rtk_position.point.y = msg->pose.pose.position.y;
    rtk_position.point.z = msg->pose.pose.position.z+0.30; //30cm from floor to base link

    this->publishDJITRK(rtk_position);

}

void ethz_msf_process_ros::optitrackOdomCallback(const nav_msgs::OdometryConstPtr &msg)
{

    geometry_msgs::PoseStamped opti_pose;
    opti_pose.header.stamp = msg->header.stamp;
    opti_pose.pose.position.x = msg->pose.pose.position.x;
    opti_pose.pose.position.y = msg->pose.pose.position.y;
    opti_pose.pose.position.z = msg->pose.pose.position.z;

    opti_pose.pose.orientation.x = msg->pose.pose.orientation.x;
    opti_pose.pose.orientation.y = msg->pose.pose.orientation.y;
    opti_pose.pose.orientation.z = msg->pose.pose.orientation.z;
    opti_pose.pose.orientation.w = msg->pose.pose.orientation.w;

    this->publishDJIPose(opti_pose);

}

void ethz_msf_process_ros::imuCallback(const sensor_msgs::ImuConstPtr &msg)
{

    if(!use_geodesic_yaw_)
    {
        if(!dji_first_yaw_measurement_)
        {
            tf::Quaternion q(msg->orientation.x, msg->orientation.y, msg->orientation.z, msg->orientation.w);
            tf::Matrix3x3 m(q);

            //convert quaternion to euler angels
            m.getRPY(dji_first_roll_, dji_first_pitch_, dji_first_yaw_);

            if(std::isnan(dji_first_yaw_))
                return;

            dji_first_yaw_measurement_ = true;
        }
    }

    this->setIMUdata(msg);

    return;
}

void ethz_msf_process_ros::setIMUdata(sensor_msgs::ImuConstPtr imu)
{
    imu_data_ = imu;
}

void ethz_msf_process_ros::getIMUdata(sensor_msgs::ImuConstPtr& imu)
{
    imu = imu_data_;
}

void ethz_msf_process_ros::DjiHeigtCallback(const std_msgs::Float32 &msg)
{
    geometry_msgs::PointStamped altitude;
    altitude.header.stamp       = ros::Time::now();
    altitude.point.x            = 0;
    altitude.point.y            = 0;
    altitude.point.z            = msg.data + 0.18;
    dji_current_sonar_height_   = msg.data + 0.18;

    this->publishAltitudeAboveTakeoff(altitude);

}

void ethz_msf_process_ros::msfOdomCallback(const nav_msgs::Odometry &msg)
{
    this->setMSFOdom(msg);
    //this->msfTfTransform(msg);
    this->aerostackPose(msg);

}

void ethz_msf_process_ros::setMSFOdom(nav_msgs::Odometry odom)
{
    msf_odom_lock_.lock();
    msf_odom_ = odom;
    msf_odom_lock_.unlock();
}

void ethz_msf_process_ros::getMSFOdom(nav_msgs::Odometry &odom)
{
    msf_odom_lock_.lock();
    odom = msf_odom_;
    msf_odom_lock_.unlock();
}

void ethz_msf_process_ros::aerostackPose(nav_msgs::Odometry msg)
{
    ethz_msf_process::dronePose estimated_pose;
    ethz_msf_process::droneSpeeds estimated_speeds;

    estimated_pose.header.stamp = msg.header.stamp;
    estimated_pose.x = msg.pose.pose.position.x;
    estimated_pose.y = msg.pose.pose.position.y;
    estimated_pose.z = msg.pose.pose.position.z;

    //converting the orientation from quaternion to roll, pitch and yaw
    tf::Quaternion q(msg.pose.pose.orientation.x,msg.pose.pose.orientation.y,
                     msg.pose.pose.orientation.z, msg.pose.pose.orientation.w);

    tf::Matrix3x3 m(q);
    double yaw, pitch, roll;
    m.getEulerYPR(yaw, pitch, roll);

    estimated_pose.pitch = pitch;
    estimated_pose.roll  = roll;
    estimated_pose.yaw   = yaw;

    //-------------------------------------------------------------------------------------------------------------
    // Estimated Speeds from the EKF
    //-------------------------------------------------------------------------------------------------------------

    //transformVector funtion requires the frame_id as well as the current time
    geometry_msgs::Vector3Stamped vin, vout;
    vin.header.stamp = msg.header.stamp;
    vin.header.frame_id = "/base_link";
    vin.vector.x = msg.twist.twist.linear.x;
    vin.vector.y = msg.twist.twist.linear.y;
    vin.vector.z = msg.twist.twist.linear.z;

    //Using tf to convert the speeds (twist.linear) messages from base_link frame to odom frame as
    //the Aerostack requires speeds in odom frame (i.e world frame)
    try{
        listener_.transformVector("/odom",ros::Time(0) ,vin, "/base_link",vout);
    }
    catch (tf::TransformException ex)
    {
        ROS_ERROR("%s",ex.what());
        ros::Duration(1.0).sleep();
    }

    estimated_speeds.dx   =  vout.vector.x;
    estimated_speeds.dy   =  vout.vector.y;
    estimated_speeds.dz   = -vout.vector.z;
    estimated_speeds.dyaw = -msg.twist.twist.angular.z;

    this->publishAerostackPose(estimated_pose);
    this->publishAerostackSpeeds(estimated_speeds);
}

void ethz_msf_process_ros::msfTfTransform(nav_msgs::Odometry msg)
{
    //    tf::Quaternion tf_quat;
    //    tf_quat.setX(msg.pose.pose.orientation.x);
    //    tf_quat.setY(msg.pose.pose.orientation.y);
    //    tf_quat.setZ(msg.pose.pose.orientation.z);
    //    tf_quat.setW(msg.pose.pose.orientation.w);

    //    static tf::TransformBroadcaster br;
    //    tf::Transform transform;
    //    transform.setOrigin(tf::Vector3(msg.pose.pose.position.x, msg.pose.pose.position.y, msg.pose.pose.position.z));
    //    transform.setRotation(tf_quat);
    //    br.sendTransform(tf::StampedTransform(transform, msg.header.stamp, "odom", "base_link"));

}

void ethz_msf_process_ros::publishVelocity(geometry_msgs::PointStamped vel)
{
    velocity_pub_.publish(vel);
}

void ethz_msf_process_ros::publishDJIPose(geometry_msgs::PoseStamped pose)
{
    dji_pose_pub_.publish(pose);
}

void ethz_msf_process_ros::publishAltitudeAboveTakeoff(geometry_msgs::PointStamped altitude)
{
    altitude_above_takeoff_pub_.publish(altitude);
}

void ethz_msf_process_ros::publishMSFOdom()
{
    ros::Rate loop_rate(200);
    while(ros::ok())
    {
        nav_msgs::Odometry odom;
        this->getMSFOdom(odom);
        msf_odom_pub_.publish(odom);
        loop_rate.sleep();
    }
}

void ethz_msf_process_ros::publishDJITRK(const geometry_msgs::PointStamped position)
{
    dji_rtk_pub_.publish(position);
}

void ethz_msf_process_ros::publishAerostackPose(ethz_msf_process::dronePose msg)
{
    aerostack_pose_pub_.publish(msg);
}

void ethz_msf_process_ros::publishAerostackSpeeds(ethz_msf_process::droneSpeeds msg)
{
    aerostack_speed_pub_.publish(msg);
}

Eigen::Vector3f ethz_msf_process_ros::ConvertToENU(geometry_msgs::Vector3Stamped msg)
{
    //converting the proper ENU velocity frame to the aerostack world frame based on the first yaw measurement
    Eigen::Vector3f dji_proper_ENU_frame;
    Eigen::Vector3f local_world_frame;
    Eigen::Matrix3f local_world_rotation_mat;

    dji_proper_ENU_frame(0) = msg.vector.x;
    dji_proper_ENU_frame(1) = msg.vector.y;
    dji_proper_ENU_frame(2) = 0;

    local_world_rotation_mat(0,0) = cos(dji_first_yaw_);
    local_world_rotation_mat(1,0) = -sin(dji_first_yaw_);
    local_world_rotation_mat(2,0) = 0;

    local_world_rotation_mat(0,1) = sin(dji_first_yaw_);
    local_world_rotation_mat(1,1) = cos(dji_first_yaw_);
    local_world_rotation_mat(2,1) = 0;

    local_world_rotation_mat(0,2) = 0;
    local_world_rotation_mat(1,2) = 0;
    local_world_rotation_mat(2,2) = 1;

    local_world_frame = local_world_rotation_mat * dji_proper_ENU_frame;

    return local_world_frame;
}


tf::Quaternion ethz_msf_process_ros::getCorrectYaw(sensor_msgs::ImuConstPtr imu_msg)
{

    tf::Quaternion q(imu_msg->orientation.x, imu_msg->orientation.y, imu_msg->orientation.z, imu_msg->orientation.w);
    tf::Matrix3x3 m(q);
    double roll, pitch, yaw;
    //convert quaternion to euler angels
    m.getRPY(roll, pitch, yaw);

    if(yaw - dji_first_yaw_ < -180.0)
        yaw = (yaw - dji_first_yaw_) + 360;
    else if(yaw - dji_first_yaw_ > 180)
        yaw = (yaw - dji_first_yaw_) - 360;
    else
        yaw = yaw - dji_first_yaw_;

    tf::Quaternion quaternion = tf::createQuaternionFromRPY(roll,pitch,yaw);

    return quaternion;

}

void ethz_msf_process_ros::geodesicFirstYawCallback(const std_msgs::Float32 &msg)
{

    dji_first_yaw_ = msg.data;

    if(std::isnan(dji_first_yaw_))
        return;

    std::cout << "dji_first_yaw geodesic: " << msg.data << std::endl;
    dji_first_yaw_measurement_ = true;

}
