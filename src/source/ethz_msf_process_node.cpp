#include "ethz_msf_process.h"

int main(int argc, char **argv)
{

    ros::init(argc, argv, ros::this_node::getName());

    ethz_msf_process_ros my_msf_process;
    my_msf_process.setUp(); 
    my_msf_process.start();

    ros::spin();
    my_msf_process.run();

    return 0;
}


