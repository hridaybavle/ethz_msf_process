#include <iostream>
#include <string>
#include <math.h>
#include <mutex>
#include <binders.h>
#include <thread>

//ROS
#include "ros/ros.h"
#include "geometry_msgs/Vector3Stamped.h"
#include "sensor_msgs/Imu.h"
#include "nav_msgs/Odometry.h"
#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include "std_msgs/Float32.h"

//robot process
#include "robot_process.h"

//Eigen
#include "eigen3/Eigen/Eigen"
#include "eigen3/Eigen/Eigen"

//tf
#include "tf_conversions/tf_eigen.h"
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>

//ros time synchronizer
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

//dynamic reconfigure
#include <dynamic_reconfigure/DoubleParameter.h>
#include <dynamic_reconfigure/Reconfigure.h>
#include <dynamic_reconfigure/Config.h>

//dji message
#include "ethz_msf_process/VOPosition.h"

//aerostack messages
#include "ethz_msf_process/dronePose.h"
#include "ethz_msf_process/droneSpeeds.h"

class ethz_msf_process_ros : public RobotProcess
{

public:
    ethz_msf_process_ros();
    ~ethz_msf_process_ros();

public:
    void ownSetUp();
    void ownStart();
    void ownStop();
    void ownRun();
    void init();

private:
    ros::NodeHandle n;

    //Subscibers
    ros::Subscriber dji_velocity_sub_;
    ros::Subscriber dji_imu_sub_;
    ros::Subscriber dji_height_sub_;
    ros::Subscriber msf_odom_sub_;
    ros::Subscriber dji_rtk_sub_;
    ros::Subscriber optitrack_odom_sub_;
    ros::Subscriber geodesic_first_yaw_sub_;


    void velocityCallback(const geometry_msgs::Vector3Stamped& msg);
    void imuCallback(const sensor_msgs::ImuConstPtr &msg);
    void DjiHeigtCallback(const std_msgs::Float32& msg);
    void msfOdomCallback(const nav_msgs::Odometry& msg);
    void msfTfTransform(nav_msgs::Odometry msg);
    void djiRTKCallback(const nav_msgs::OdometryConstPtr& msg);
    void optitrackOdomCallback(const nav_msgs::OdometryConstPtr& msg);
    void geodesicFirstYawCallback(const std_msgs::Float32& msg);

    tf::TransformListener listener_;

    //message synchronizers for gps and imu
    message_filters::Subscriber<sensor_msgs::Imu> dji_imu_snyn_sub_;
    message_filters::Subscriber<nav_msgs::Odometry> gps_enu_sub_;

    typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::Imu,
    nav_msgs::Odometry> SyncPolicy;
    message_filters::Synchronizer<SyncPolicy> sync;

    void imuGPSSyncCallback(const sensor_msgs::ImuConstPtr &imu_msg,
                            const nav_msgs::OdometryConstPtr &gps_msg);


    //message synchronizers for vo and imu
    message_filters::Subscriber<sensor_msgs::Imu> dji_vo_imu_sync_sub_;
    message_filters::Subscriber<ethz_msf_process::VOPosition> vo_pose_sub_;

    typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::Imu,
    ethz_msf_process::VOPosition> SyncPolicyIMUVO;
    message_filters::Synchronizer<SyncPolicyIMUVO> sync_imu_vo;

    void IMUVOsyncCallback(const sensor_msgs::ImuConstPtr &imu_msg,
                           const ethz_msf_process::VOPositionConstPtr &vo_msg);

    //Publishers
    ros::Publisher velocity_pub_;
    inline void publishVelocity(geometry_msgs::PointStamped vel);

    ros::Publisher dji_pose_pub_;
    inline void publishDJIPose(geometry_msgs::PoseStamped pose);

    ros::Publisher altitude_above_takeoff_pub_;
    inline void publishAltitudeAboveTakeoff(geometry_msgs::PointStamped altitude);

    ros::Publisher msf_odom_pub_;
    inline void publishMSFOdom();

    ros::Publisher dji_rtk_pub_;
    inline void publishDJITRK(const geometry_msgs::PointStamped position);

    nav_msgs::Odometry msf_odom_;
    inline void setMSFOdom(nav_msgs::Odometry odom);
    inline void getMSFOdom(nav_msgs::Odometry& odom);

    ros::Publisher aerostack_pose_pub_;
    inline void publishAerostackPose(ethz_msf_process::dronePose msg);

    ros::Publisher aerostack_speed_pub_;
    inline void publishAerostackSpeeds(ethz_msf_process::droneSpeeds msg);

    inline void setIMUdata(sensor_msgs::ImuConstPtr imu);
    inline void getIMUdata(sensor_msgs::ImuConstPtr& imu);
    sensor_msgs::ImuConstPtr imu_data_;

    double vo_position_x_, vo_position_y_, vo_position_z_;


    //publisher thread
private:
    std::thread *msf_odom_pub_thread;
    std::mutex msf_odom_lock_;


private:

    bool dji_first_yaw_measurement_;
    bool dji_first_yaw_geodesic_coordinates_;
    double dji_first_roll_, dji_first_pitch_, dji_first_yaw_;
    double dji_first_yaw_geodesic_;
    double prev_time_; double current_time_;
    bool start_calcualating_dt_;

private:

    bool use_vo_data_, use_global_positioning_data_;
    bool use_rtk_position_, use_rtk_pose_, use_gps_pose_;
    bool use_optitrack_data_;
    bool use_geodesic_yaw_;
    bool is_first_vo_z_msg_;
    float first_vo_z_msg_;
    float dji_current_sonar_height_;

    tf::Quaternion getCorrectYaw(sensor_msgs::ImuConstPtr imu_msg);
    Eigen::Vector3f ConvertToENU(geometry_msgs::Vector3Stamped msg);
    void aerostackPose(nav_msgs::Odometry msg);

};





